FROM python:3.9-slim

RUN apt-get update \
    && apt-get -y install git jq \
    && rm -rf /var/lib/apt/lists/*
RUN groupadd --gid 1000 -r python && useradd --uid 1000 -r -g python python
RUN pip install poetry nox nox-poetry python-semantic-release

WORKDIR /home/python
RUN chown -R python:python /home/python
USER python
