"""Nox sessions."""
import sys
from pathlib import Path
from textwrap import dedent

import nox

try:
    from nox_poetry import Session
    from nox_poetry import session
except ImportError:
    message = f"""\
    Nox failed to import the 'nox-poetry' package.

    Please install it using the following command:

    {sys.executable} -m pip install nox-poetry"""
    raise SystemExit(dedent(message)) from None


package = "{{cookiecutter.package_name}}"
python_versions = ["3.8", "3.9", "3.10"]
nox.needs_version = ">= 2021.6.6"
nox.options.sessions = "lint", "safety", "tests"
locations = "src", "tests", "noxfile.py"


TEST_DEPS = [
    "pytest",
    "coverage[toml]",
    "pytest-cov",
    "pytest-mock",
]


@session(python="3.9")
def black(session: Session) -> None:
    """Run black code formatter."""
    args = session.posargs or locations
    session.install("black")
    session.run("black", *args)


@session(python=python_versions)
def lint(session: Session) -> None:
    """Lint using flake8."""
    args = session.posargs or locations
    session.install(
        "flake8",
        "flake8-annotations",
        "flake8-bandit",
        "flake8-black",
        "flake8-bugbear",
        "flake8-docstrings",
        "flake8-import-order",
        "flake8-json",
        "flake8-codeclimate",
    )
    session.run("flake8", *args)


@session(python=python_versions[-1])
def safety(session: Session) -> None:
    """Scan dependencies for insecure packages."""
    requirements = session.poetry.export_requirements()
    session.install("safety")
    session.run("safety", "check", "--full-report", f"--file={requirements}")


@session(python=python_versions)
def tests(session: Session) -> None:
    """Run the test suite."""
    args = session.posargs or ["--cov", "-m", "not e2e"]
    session.run("poetry", "install", "--no-dev", external=True)
    session.install(*TEST_DEPS)
    try:
        session.run("pytest", *args)
    finally:
        if session.interactive:
            session.notify("coverage", posargs=[])


@session(python=python_versions[-1])
def coverage(session: Session) -> None:
    """Produce the coverage report."""
    args = session.posargs or ["xml"]

    session.install("coverage[toml]")

    if not session.posargs and any(Path().glob(".coverage.*")):
        session.run("coverage", "combine")

    session.run("coverage", *args)


@session(python=python_versions)
def typeguard(session: Session) -> None:
    """Runtime type checking using Typeguard."""
    session.run("poetry", "install", "--no-dev", external=True)
    session.install("typeguard", "pygments", *TEST_DEPS)
    session.run("pytest", f"--typeguard-packages={package}", *session.posargs)
